import sys
import time
import requests
import tkinter as tk
from tkinter import messagebox

from datetime import datetime
from bs4 import BeautifulSoup

class params:
    def __init__(self) -> None:
        self.dowStat = {}

def main():
    # initiate params
    scrapeMsg = params()


    url = "https://recwell.purdue.edu/program/getprogramdetails?courseid=5c2ae41f-cff3-4476-a6bc-a1c450a854c1&semesterid=39a3b8af-5c1b-42b6-89e2-ed3d4997d49e"
    # set the headers like we are a browser,
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

    while True:
        # download the homepage
        response = requests.get(url, headers=headers)
        # parse the downloaded homepage and grab all text, then,
        soup = BeautifulSoup(response.text, "lxml")
        # print(soup)
        # sys.exit()
        # Get current time
        now = datetime.now()
        print("\n-------------------{}-------------------\n".format(now.strftime('%y-%m-%d %H:%M:%S')))
        for noodle in soup:
            line = noodle.text.strip()
            
            if "Program Instances" in line:
                # parse out non-relevant data
                lines = line.split("Program Instances")[1].replace("\n", "_").split("Register")
                count = 0
                for line in lines:
                    if line.endswith('e___'):
                        count+=1
                
                for i in range(count):
                    # init local params
                    openDate = lines[i].replace("___", "|").replace("_", "|").replace("_", "").split("|")[-4:-1]

                    progSlot = openDate[0].split(",")[0] + " [{}]".format(openDate[1])
                    timerange = openDate[1]
                    availability = openDate[2]

                    # init params for saving differences
                    if progSlot not in scrapeMsg.dowStat:
                        scrapeMsg.dowStat[progSlot] = availability

                    print("{} availability @ {}: {}".format(progSlot.split(" [")[0],timerange,availability))
                    if availability == scrapeMsg.dowStat[progSlot]:
                        pass
                    else:
                        try:
                            tk.messagebox.showinfo("Congratulations", "OwO {} CHANGE DETECTED".format(progSlot.upper()))
                        except Exception as e:
                            print(e)
                        scrapeMsg.dowStat[progSlot] = availability
                        pass
                
                time.sleep(30)

if __name__ == "__main__":
    main()